#pragma once

//	https://www.esri.com/library/whitepapers/pdfs/shapefile.pdf

/*
Value Shape	Type
--------------------------
0		Null Shape
1		Point
3		PolyLine
5		Polygon
8		MultiPoint
11		PointZ
13		PolyLineZ
15		PolygonZ
18		MultiPointZ
21		PointM
23		PolyLineM
25 		PolygonM
28		MultiPointM
31		MultiPatch

File Length: The value for file length is the total length of the file in 16-bit words 
(including the fifty 16-bit words that make up the header).

All the non-Null shapes in a shapefile are required to be of the same shape type.
*/

#define FILE_HEADER_LEN	100

class ShapefileHeader
{
public:
	int			fileCode;
	int			B04;
	int			B08;
	int			B12;
	int			B16;
	int			B20;
	int			fileLength;			//Big Endian ^
	int			version;			//Little Endian v
	int			shapeType;
	long double	minX;
	long double	minY;
	long double	maxX;
	long double	maxY;
	long double	minZ;
	long double	maxZ;
	long double	minM;
	long double	maxM;
};
/*
Endianness:
With big-endian the most-significant byte of a word is stored 
at a particular memory address and the subsequent bytes are stored 
in the following higher memory addresses, the least significant byte 
thus being stored at the highest memory address.
Little-endian format reverses the order and stores the least-significant byte 
at the lower memory address with the most significant byte being stored 
at the highest memory address.
*/
class RecordHeader				//Big Endian
{
public:
	int		recordNumber;		//Begins at 1, NOT ZERO
	int		contentLength;
};

class Point						//ShapeType = 0
{
public:
	int			shape;
	long double	x;
	long double	y;
	int			header;
	bool		valid;
};
/*
The content length for a record is the length of the record contents section measured in
16-bit words. Each record, therefore, contributes (4 + content length) 16-bit words
toward the total length of the file, as stored at Byte 24 in the file header.
*/

class MultiPoint
{
public:
	double				box[4];		//Bounding Box is stored in the order Xmin, Ymin, Xmax, Ymax
	int					numPoints;
	std::vector<Point>	points;		// [numPoints]
};

struct vpart
{
	int					index;
	bool				valid;
};

class PolyLine
{
public:
	long double			box[4];		// Bounding Box for the PolyLine stored in the order Xmin, Ymin, Xmax, Ymax.
	int					numParts;	// The number of parts in the PolyLine.
	int					numPoints;	// The total number of points for all parts.
	std::vector<vpart>	parts;		// An array of length NumParts. Stores, for each PolyLine, the index of its first point in the points array. Array indexes are with respect to 0.
	std::vector<Point>	points;		// An array of length NumPoints. The points for each part in the PolyLine are stored end to end. The points for Part 2 follow the points for Part 1, and so on. The parts array holds the array index of the starting point for each part. There is no delimiter in the points array between parts.
	int					shape;
	int					header;
	bool				valid;
};

class Polygon
{
public:
	long double			box[4];		// The Bounding Box for the polygon stored in the order Xmin, Ymin, Xmax, Ymax.
	int					numParts;	// The number of rings in the polygon.
	int					numPoints;	// The total number of points for all rings.
	std::vector<vpart>	parts;		// An array of length NumParts. Stores, for each ring, the index of its first point in the points array. Array indexes are with respect to 0.
	std::vector<Point>	points;		// An array of length NumPoints. The points for each ring in the polygon are stored end to end. The points for Ring 2 follow the points for Ring 1, and so on. The parts array holds the array index of the starting point for each ring. There is no delimiter in the points array between rings.
	int					shape;
	int					header;
	bool				valid;
};

class PointM
{
public:
	double x;
	double y;
	double m;
};

class MultiPointM
{
public:
	double box[4];				// Bounding Box for the MultiPointM stored in the order Xmin, Ymin, Xmax, Ymax
	int numPoints;
	std::vector<PointM> points;	// An array of Points of length NumPoints
	double mRange[2];			// The minimum and maximum measures for the MultiPointM stored in the order Mmin, Mmax
	std::vector<double> mArray;	// An array of measures of length NumPoints
};

class PolyLineM
{
public:
	double box[4];				// Bounding Box for the PolyLineM stored in the order Xmin, Ymin, Xmax, Ymax.
	int numParts;				// The number of parts in the PolyLineM
	int numPoints;				// The total number of points for all parts
	std::vector<int> parts;		// An array of length NumParts. Stores, for each part, the index of its first point in the points array. Array indexes are with respect to 0.
	std::vector<PointM> points;	//  An array of length NumPoints. The points for each part in the PolyLineM are stored end to end. The points for Part 2 follow the points for Part 1, and so on. The parts array holds the array index of the starting point for each part.There is no delimiter in the points array between parts.
	double mRange[2];			// The minimum and maximum measures for the MultiPointM stored in the order Mmin, Mmax
	std::vector<double> mArray;	// An array of length NumPoints. The measures for each part in the PolyLineM are stored end to end. The measures for Part 2 follow the measures for Part 1, and so on. The parts array holds the array index of the starting point for each part.There is no delimiter in the measure array between parts
};

class PolygonM
{
public:
	double box[4];				// Bounding Box for the PolygonM stored in the order Xmin, Ymin, Xmax, Ymax.
	int numParts;				// The number of rings in the PolygonM
	int numPoints;				// The total number of points for all rings.
	std::vector<int> parts;		// An array of length NumParts. Stores, for each ring, the index of its first point in the points array. Array indexes are with respect to 0.
	std::vector<PointM> points;	// An array of length NumPoints. The points for each ring in the PolygonM are stored end to end. The points for Ring 2 follow the points for Ring 1, and so on.The parts array holds the array index of the starting point for each ring.There is no delimiter in the points array between rings.
	double mRange[2];			// The minimum and maximum measures for the PolygonM stored in the order Mmin, Mmax.
	std::vector<double> mArray;	// An array of length NumPoints. The measures for each ring in the PolygonM are stored end to end. The measures for Ring 2 follow the measures for Ring 1, and so on.The parts array holds the array index of the starting measure for each ring.There is no delimiter in the measure array between rings.
};

class PointZ
{
public:
	double x;
	double y;
	double m;
	double z;
};

/*
The Bounding Box is stored in the order Xmin, Ymin, Xmax, Ymax.
The bounding Z Range is stored in the order Zmin, Zmax. Bounding M Range is stored
in the order Mmin, Mmax.
*/
class MultiPointZ
{
public:
	double box[4];
	int numPoints;
	std::vector<PointZ> points;
	double zRange[2];
	std::vector<double> zArray;
	double mRange[2];
	std::vector<double> mArray;
};

class PolyLineZ
{
public:
	double box[4];
	int numPoints;
	int numParts;
	std::vector<int> parts;
	std::vector<PointZ> points;
	double zRange[2];
	std::vector<double> zArray;
	double mRange[2];
	std::vector<double> mArray;
};

/*
The following are important notes about PolygonZ shapes:
The rings are closed (the first and last vertex of a ring MUST be the same).
The order of rings in the points array is not significant.
*/
class PolygonZ
{
public:
	double box[4];
	int numParts;
	int numPoints;
	std::vector<int> parts;
	std::vector<PointZ> points;
	double zRange[2];
	std::vector<double> zArray;
	double mRange[2];
	std::vector<double> mArray;
};

/*
Value		Part Type
0			Triangle Strip
1			Triangle Fan
2			Outer Ring
3			Inner Ring
4			First Ring
5			Ring
*/
/*
The following are important notes about MultiPatch shapes:
If a part is a ring, it must be closed (the first and last vertex of a ring MUST be the
same).
The order of parts that are rings in the points array is significant: Inner Rings must
follow their Outer Ring; a sequence of Rings representing a single surface patch must
start with a ring of the type First Ring.
Parts can share common boundaries, but parts must not intersect and penetrate each
other.
*/
class MultiPatch
{
public:
	double box[4];
	int numParts;
	int numPoints;
	std::vector<int> parts;
	std::vector<int> partTypes;
	std::vector<PointZ> points;
	double zRange[2];
	std::vector<double> zArray;
	double mRange[2];
	std::vector<double> mArray;
};
