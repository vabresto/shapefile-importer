#pragma once

#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>
#include <bitset>
#include <intrin.h>
#include <stdint.h>
#include <math.h>
#include <iomanip>

#include <SFML\Graphics.hpp>
#include "ShapefileClass.h"
#include "IndexFileClass.h"

// /Byte Swap Reference - Convert Endians
/*
http://stackoverflow.com/questions/105252/how-do-i-convert-between-big-endian-and-little-endian-values-in-c

If you're using Visual C++ include intrin.h and call the following functions:

For 16 bit numbers:	unsigned short _byteswap_ushort(unsigned short value);
For 32 bit numbers:	unsigned long _byteswap_ulong(unsigned long value);
For 64 bit numbers:	unsigned __int64 _byteswap_uint64(unsigned __int64 value);
The 8 bit numbers (chars) don't need to be converted.

For floats and doubles it's more difficult as with plain integers as these may or not may be in the 
host machines byte-order. You can get little-endian floats on big-endian machines and vice versa.
Other compilers have similar intrinsics as well.
*/

#define	BUF_SIZE				32768		// maximum buffer size
#define	NAME_SIZE				128			// name size
#define	BUF_SEP					'*'			// separator
#define	CR						0x0D		// carriage-return
#define	LF						0x0A		// line-feed
extern std::string				fileBase;			//File name without extension (all related shapefiles have same non-extension name)
extern std::string				fileLocation;		//File location - everything up to and including the folder it is in

extern char*					_fileName;
extern std::string				nodeGraph;
extern std::vector<FILE*>		filesList;
extern std::vector<FILE*>		indexList;
extern std::vector<std::string>	fileNameList;

#define	SCALE					100
#define	EPSILON					0.00000000001
#define COMPRESS				1
// Default filtering box
#define	MINX					70000
#define	MINY					5400000
#define	MAXX					90000
#define	MAXY					5600000
extern long						recValid;
extern long						recDiscard;

struct PathingNode
{
	unsigned int				nodeID;			//ID corresponding with ID of MainNode nodes
	double						fCost;
	double						gCost;
	double						hCost;
	double						x;
	double						y;
	PathingNode*				parent;
	std::vector<PathingNode*>	adjacentsList;
};

struct MainNode
{
	unsigned int				nodeID;
	unsigned int				header;
	double						x;
	double						y;
	std::vector<MainNode*>		adjacentsList;
};

// array[0] contains Record #1
extern std::vector<Point>			pointList;
extern std::vector<PolyLine>		polyLineList;
extern std::vector<Polygon>			polyDataList;
extern std::vector<ShapefileHeader>	headerList;
extern std::vector<sf::Color>		colorList;
extern std::vector<sf::Texture>		textureList;
extern std::vector<MainNode*>		nodesList;
extern std::vector<sf::Sprite>		drawPointsList;

const std::string				fileExtMain = ".shp";
const std::string				fileExtIndex = ".shx";
const std::string				fileExtDBase = ".dbf";

extern sf::RenderWindow			mainWindow;
extern sf::View					mainView;

/* Enum for displayDebug
0 - Display no text
1 - Display 'regular' text
2 - Display header log
4 - Display header & record header logs
8 - Display record header contents
16 - Display all contents
  ~
31 - Display ALL logs
*/
enum _showDebug
{
	SHOW_TEXT		= 1,	// 0000001
	SHOW_HEADER		= 2,	// 0000010
	SHOW_RECORD		= 4,	// 0000100
	SHOW_CONTENT	= 8,	// 0001000
	SHOW_POINTS		= 16,	// 0010000
	SHOW_SEARCH		= 32,	// 0100000
	SHOW_MOUSE		= 64,	// 1000000
	SHOW_MAX
};
const int						displayDebug = SHOW_TEXT + SHOW_HEADER + SHOW_MOUSE;

struct LineSegment
{
	unsigned int				header;
	unsigned int				record;
	unsigned int				part;
	sf::VertexArray				vertexList;
	std::vector<sf::Sprite>		spriteList;
};

extern std::vector<LineSegment>	routesList;
extern sf::VertexArray			pathLinesList;
extern bool						exportNodes;
extern bool						doDrawing;
extern bool						doCompress;
extern bool						doSaveToFile;
extern bool						showNodes;

extern void aStarPathFind(MainNode* _start, MainNode* _target);
extern double getDistance(PathingNode* _init, PathingNode* _comp);
extern double getDistance(PathingNode* _init, MainNode* _comp);
extern double getDistance(sf::Sprite _init, MainNode* _comp);
extern double getDistance(sf::Vector2f _init, MainNode* _comp);
extern std::vector<PathingNode*> getNeighbours(MainNode* _current);
extern std::vector<MainNode*> retracePathPointer(PathingNode* _start, PathingNode* _end);
extern void loadNodeGraph(std::string _nodeGraph);
extern void exportNodeGraph(std::vector<MainNode*> _list);
extern void relinkNodes(std::vector<MainNode*> _nodesList);

