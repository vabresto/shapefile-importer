#pragma once

/*
The offset of a record in the main file is the number of 16-bit words from 
the start of the main file to the first byte of the record header for the record. 
Thus, the offset for the first record in the main file is 50, given the 100-byte header.
The content length stored in the index record is the same as the value stored in the main 
file record header.
*/

class Record
{
	int offset;				//Big Endian
	int contentLength;		//Big Endian
};
