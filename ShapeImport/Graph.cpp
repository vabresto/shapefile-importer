#include "Include\GlobalVariables.h"


////////////////////////////////////////////////////////////////////////////////
bool canCompress(double _initx, double _inity, double _compx, double _compy)
{
	// Check if distance on each axis between the points is less than a constant
	if ((abs(_initx - _compx) < COMPRESS) && (abs(_inity - _compy) < COMPRESS))
		return true;
	else
		return false;
}

////////////////////////////////////////////////////////////////////////////////
bool notInList(unsigned int nodeID, std::vector<MainNode*> list)
{
	unsigned int	cntAdj = 0;
	bool			retval = true;

	for (cntAdj = 0; cntAdj < list.size(); cntAdj++)
	{
		if (list[cntAdj]->nodeID == nodeID)
		{
			retval = false;
			break;
		}
	}
	return retval;
}

////////////////////////////////////////////////////////////////////////////////
void generateNodes(unsigned int _index)
{
	MainNode*		node = NULL;
	unsigned int	cntSegment = 0;
	unsigned int	cntNode = 0;
	unsigned int	pointID = 0;
	sf::Clock		timer;
	sf::Time		generTime;
	sf::Time		prelimTime;

	// Create linked-list of nodes within segments
	timer.restart();
	for (cntSegment = 0; cntSegment < routesList.size(); cntSegment++)
	{
		// Check if segment belongs to this header
		if (routesList[cntSegment].header == _index)
		{
			for (cntNode = 0; cntNode < routesList[cntSegment].vertexList.getVertexCount(); cntNode++)
			{
				// Allocate new node
				node = new MainNode;
				node->nodeID = pointID;
				node->x = routesList[cntSegment].vertexList[cntNode].position.x;
				node->y = routesList[cntSegment].vertexList[cntNode].position.y;
				node->header = _index;

				// Set node as adjacent to previous node within segment
				if (cntNode > 0)
				{
					nodesList[(nodesList.size() - 1)]->adjacentsList.push_back(node);
					node->adjacentsList.push_back(nodesList[nodesList.size() - 1]);
				}
				// Add node to list
				nodesList.push_back(node);
				pointID++;
			}
		}
	}
	// Get elapsed time
	generTime = timer.getElapsedTime();
	if (displayDebug & SHOW_TEXT)
	{
		std::cout << "Finished generating " << nodesList.size() << " nodes in " << generTime.asMilliseconds() << " milliseconds" << std::endl;
	}

	// Remove empty nodes
	timer.restart();
	for (cntNode = 0; cntNode < nodesList.size(); cntNode++)
	{
		if (nodesList[cntNode]->adjacentsList.size() <= 0)
		{
			delete nodesList[cntNode];
			nodesList[cntNode] = NULL;
			nodesList.erase(nodesList.begin() + cntNode);
			cntNode--;
		}
	}
	// Get elapsed time
	prelimTime = timer.getElapsedTime();
	if (displayDebug & SHOW_TEXT)
	{
		std::cout << "Finished clearing nodes (remaining " << nodesList.size() << ") in " << prelimTime.asMilliseconds() << " milliseconds" << std::endl;
	}

	return;
}

////////////////////////////////////////////////////////////////////////////////
void compressNodes(void)
{
	unsigned int	cntNode = 0;
	unsigned int	cntNext = 0;
	unsigned int	cntAdj = 0;
	unsigned int	cntFind = 0;
	int				cntDots = 0;
	sf::Clock		timer;
	sf::Time		clearTime;

	if (doCompress)
	{
		if (displayDebug & SHOW_TEXT)
		{
			std::cout << "Compressing graph nodes. Working ";
		}
		// Compare each node in list belonging to a route
		timer.restart();
		if (nodesList.size() > 0)
		{
			for (cntNode = 0; cntNode < nodesList.size() - 1; cntNode++)
			{
				// With next nodes
				for (cntNext = cntNode + 1; cntNext < nodesList.size(); cntNext++)
				{
					// Check if nodes are so closed that can be compressed into one node
					if (canCompress(nodesList[cntNode]->x, nodesList[cntNode]->y,
						nodesList[cntNext]->x, nodesList[cntNext]->y))
					{
						// For each node in the adjacents list
						for (cntAdj = 0; cntAdj < nodesList[cntNext]->adjacentsList.size(); cntAdj++)
						{
							// Copy adjacency linked to duplicate node, excepting link back to current node or if is already in list
							for (cntFind = 0; cntFind < nodesList[cntNext]->adjacentsList[cntAdj]->adjacentsList.size(); cntFind++)
							{
								// First update the link of adjacent node to point to current node
								if (nodesList[cntNext]->adjacentsList[cntAdj]->adjacentsList[cntFind]->nodeID == nodesList[cntNext]->nodeID)
								{
									// Remove it from adjacent list
									nodesList[cntNext]->adjacentsList[cntAdj]->adjacentsList.erase(nodesList[cntNext]->adjacentsList[cntAdj]->adjacentsList.begin() + cntFind);
									// Exclude link back to itself and check if adjacent node is not yet in list
									if ((nodesList[cntNext]->adjacentsList[cntAdj]->nodeID != nodesList[cntNode]->nodeID) &&
										(notInList(nodesList[cntNext]->adjacentsList[cntAdj]->nodeID, nodesList[cntNode]->adjacentsList)))
									{
										// Add current node
										nodesList[cntNext]->adjacentsList[cntAdj]->adjacentsList.push_back(nodesList[cntNode]);
									}
								}
								if (displayDebug & SHOW_TEXT)
								{
									// Get elapsed time
									clearTime = timer.getElapsedTime();
									if ((int)floor(clearTime.asSeconds()) > cntDots)
									{
										cntDots = (int)floor(clearTime.asSeconds());
										std::cout << ".";
									}
								}
							}
							// Exclude link back to itself and check if adjacent node is not yet in list
							if ((nodesList[cntNext]->adjacentsList[cntAdj]->nodeID != nodesList[cntNode]->nodeID) &&
								(notInList(nodesList[cntNext]->adjacentsList[cntAdj]->nodeID, nodesList[cntNode]->adjacentsList)))
							{
								// Add adjacent linked to duplicate node to this current node
								nodesList[cntNode]->adjacentsList.push_back(nodesList[cntNext]->adjacentsList[cntAdj]);
							}
						}
						// Delete duplicate node
						delete nodesList[cntNext];
						nodesList[cntNext] = NULL;
						nodesList.erase(nodesList.begin() + cntNext);
						cntNext--;
					}
				}
			}
		}
		// Get elapsed time
		clearTime = timer.getElapsedTime();
		if (displayDebug & SHOW_TEXT)
		{
			std::cout << std::endl;
			std::cout << "Finished clearing duplicate nodes (remaining " << nodesList.size() << ") in " << clearTime.asMilliseconds() << " milliseconds" << std::endl;
		}
	}

	return;
}

////////////////////////////////////////////////////////////////////////////////
void saveNodesToFile(void)
{
	unsigned int	cntNode = 0;
	unsigned int	cntAdj = 0;
	unsigned int	cntInval = 0;
	sf::Clock		timer;
	sf::Time		saveTime;
	std::ofstream	outFile;

	if (doSaveToFile)
	{
		// Output data to sample log file
		timer.restart();
		outFile.open("logRoads.txt");
		outFile << "==============================" << std::endl;
		outFile << "Total nodes: " << nodesList.size() << std::endl;
		outFile << "==============================" << std::endl << std::endl;
		for (cntNode = 0; cntNode < nodesList.size(); cntNode++)
		{
			outFile << "Node: " << cntNode << " with ID: " << nodesList[cntNode]->nodeID << " (" << nodesList[cntNode]->x << ", " << nodesList[cntNode]->y << ")" << std::endl;
			outFile << "Adjacencies:" << std::endl;
			for (cntAdj = 0; cntAdj < nodesList[cntNode]->adjacentsList.size(); cntAdj++)
			{
				if (nodesList[cntNode]->adjacentsList[cntAdj]->nodeID > 999999)
				{
					cntInval++;
				}
				outFile << "\tNode: " << cntAdj << " with ID: " << nodesList[cntNode]->adjacentsList[cntAdj]->nodeID << " (" << nodesList[cntNode]->adjacentsList[cntAdj]->x << ", " << nodesList[cntNode]->adjacentsList[cntAdj]->y << ")" << std::endl;
			}
			outFile << "~~~" << std::endl;
		}
		// Get elapsed time
		saveTime = timer.getElapsedTime();
		outFile << "Number of nodes with invalid Id: " << cntInval << std::endl << std::endl;
		outFile << "Finished saving all data to file in " << saveTime.asMilliseconds() << " milliseconds" << std::endl;
		outFile.close();

		if (displayDebug & SHOW_TEXT)
		{
			std::cout << "Finished saving all data to file in " << saveTime.asMilliseconds() << " milliseconds" << std::endl;
		}
	}

	return;
}

////////////////////////////////////////////////////////////////////////////////
void aStarPathFind(MainNode* _start, MainNode* _target)
{
	std::vector<PathingNode*>	openList;
	std::vector<PathingNode*>	closedList;
	PathingNode*				startNode;
	PathingNode*				currentNode;
	MainNode*					currentMainNode;
	double						newMoveCost = 0.0;
	unsigned int				cntNode = 0;
	unsigned int				cntAdj = 0;
	int							cntDots = 0;
	bool						pathFound = false;
	bool						inList = false;
	sf::Clock					aStarTimer;

	aStarTimer.restart();

	startNode = new PathingNode;
	startNode->x = _start->x;
	startNode->y = _start->y;
	startNode->nodeID = _start->nodeID;
	startNode->parent = NULL;
	startNode->gCost = 0.0;
	startNode->hCost = 0.0;
	startNode->fCost = 0.0;

	if (displayDebug & SHOW_TEXT)
	{
		std::cout << "Searching optimum path ";
	}

	openList.push_back(startNode);
	while (openList.size() > 0)
	{
		currentNode = openList[0];

		// Search in open list the node with minimum cost
		for (cntNode = 0; cntNode < openList.size(); cntNode++)
		{
			if ((openList[cntNode]->fCost < currentNode->fCost) ||
				((openList[cntNode]->fCost == currentNode->fCost) && (openList[cntNode]->hCost < currentNode->hCost)))
			{
				currentNode = openList[cntNode];
			}
		}
		if (displayDebug & SHOW_SEARCH)
		{
			std::cout << "Searching node: " << currentNode->nodeID << std::endl;
		}
		else if (displayDebug & SHOW_TEXT)
		{
			// Check elapsed time every 100 miliseconds
			if ((int)floor(10 * aStarTimer.getElapsedTime().asSeconds()) > cntDots)
			{
				cntDots = (int)floor(10 * aStarTimer.getElapsedTime().asSeconds());
				std::cout << ".";
			}
		}

		// Search in main list the current node Id
		for (cntNode = 0; cntNode < nodesList.size(); cntNode++)
		{
			if (nodesList[cntNode]->nodeID == currentNode->nodeID)
			{
				currentMainNode = nodesList[cntNode];
				break;
			}
		}

		// Add current node to list of nodes investigated
		closedList.push_back(currentNode);
		// Erase current node from list of unexplored nodes
		for (cntNode = 0; cntNode < openList.size(); cntNode++)
		{
			if (openList[cntNode]->nodeID == currentNode->nodeID)
			{
				openList.erase(openList.begin() + cntNode);
				cntNode--;
			}
		}

		// Check if path was found
		if ((currentNode->x == _target->x) &&
			(currentNode->y == _target->y))
		{
			if (displayDebug & SHOW_TEXT)
			{
				std::cout << std::endl;
				std::cout << "Done in " << aStarTimer.getElapsedTime().asMilliseconds() << " milliseconds" << std::endl;
			}

			retracePathPointer(startNode, currentNode);
			while (!closedList.empty())
			{
				delete closedList.back(), closedList.pop_back();
			}
			return;
		}

		// Get current node neighbours
		currentNode->adjacentsList = getNeighbours(currentMainNode);

		// For each adjacent node
		for (cntAdj = 0; cntAdj < currentNode->adjacentsList.size(); cntAdj++)
		{
			// Check if is already in closed list
			inList = false;
			for (cntNode = 0; cntNode < closedList.size(); cntNode++)
			{
				if (closedList[cntNode]->nodeID == currentNode->adjacentsList[cntAdj]->nodeID)
				{
					inList = true;
					break;
				}
			}
			if (inList)
			{
				// Not a candidate anymore, check next adjacent
				continue;
			}
			// Check if is in open list
			inList = false;
			for (cntNode = 0; cntNode < openList.size(); cntNode++)
			{
				if (openList[cntNode]->nodeID == currentNode->adjacentsList[cntAdj]->nodeID)
				{
					inList = true;
					break;
				}
			}

			// Calculate new cost
			newMoveCost = currentNode->gCost + getDistance(currentNode, currentNode->adjacentsList[cntAdj]);
			// If new cost is shorter OR neighbour not in list
			if ((newMoveCost < currentNode->adjacentsList[cntAdj]->gCost) ||
				(!inList))
			{
				currentNode->adjacentsList[cntAdj]->parent = currentNode;
				currentNode->adjacentsList[cntAdj]->gCost = newMoveCost;
				currentNode->adjacentsList[cntAdj]->hCost = getDistance(currentNode->adjacentsList[cntAdj], _target);
				currentNode->adjacentsList[cntAdj]->fCost = currentNode->adjacentsList[cntAdj]->gCost + currentNode->adjacentsList[cntAdj]->hCost;

				if (!inList)
				{
					openList.push_back(currentNode->adjacentsList[cntAdj]);
				}
			}
		}
	}

	if (displayDebug & SHOW_TEXT)
	{
		std::cout << std::endl;
		std::cout << "No path found after " << aStarTimer.getElapsedTime().asMilliseconds() << " milliseconds" << std::endl;
	}
	while (!closedList.empty())
	{
		delete closedList.back(), closedList.pop_back();
	}
	return;
}

////////////////////////////////////////////////////////////////////////////////
double getDistance(PathingNode* _init, PathingNode* _comp)
{
	return sqrt(pow((_init->x - _comp->x), 2) + pow((_init->y - _comp->y), 2));
}

////////////////////////////////////////////////////////////////////////////////
double getDistance(PathingNode* _init, MainNode* _comp)
{
	return sqrt(pow((_init->x - _comp->x), 2) + pow((_init->y - _comp->y), 2));
}

////////////////////////////////////////////////////////////////////////////////
double getDistance(sf::Sprite _init, MainNode* _comp)
{
	///NOTE: The y value of _init is negatve here to reflect that it has the opposite sign in reality
	return sqrt(pow((_init.getPosition().x - _comp->x), 2) + pow((_init.getPosition().y - _comp->y), 2));
}

////////////////////////////////////////////////////////////////////////////////
double getDistance(sf::Vector2f _init, MainNode* _comp)
{
	return sqrt(pow((_init.x - _comp->x), 2) + pow((_init.y - _comp->y), 2));
}

////////////////////////////////////////////////////////////////////////////////
std::vector<PathingNode*> getNeighbours(MainNode* _current)
{
	std::vector<PathingNode*>	neighboursList;
	PathingNode*				node = NULL;
	unsigned int				cntAdj = 0;

	for (cntAdj = 0; cntAdj < _current->adjacentsList.size(); cntAdj++)
	{
		node = new PathingNode;
		node->x = _current->adjacentsList[cntAdj]->x;
		node->y = _current->adjacentsList[cntAdj]->y;
		node->nodeID = _current->adjacentsList[cntAdj]->nodeID;
		node->parent = NULL;
		node->gCost = 1000000;
		node->hCost = 1000000;
		node->fCost = 1000000;

		neighboursList.push_back(node);
	}

	return neighboursList;
}

////////////////////////////////////////////////////////////////////////////////
std::vector<MainNode*> retracePathPointer(PathingNode* _start, PathingNode* _end)
{
	std::vector<MainNode*>	path;
	PathingNode*			currentNode = NULL;
	MainNode*				node = NULL;
	sf::Vertex				vertex;

	if (displayDebug & SHOW_TEXT)
	{
		std::cout << "Path back through nodes: ";
	}

	if (doDrawing)
	{
		pathLinesList.clear();
	}

	// Trace backwards
	currentNode = _end;
	while (currentNode->nodeID != _start->nodeID)
	{
		node = new MainNode;
		node->nodeID = currentNode->nodeID;
		node->x = currentNode->x;
		node->y = currentNode->y;
		node->header = 0;

		if (doDrawing)
		{
			vertex.position.x = (float)currentNode->x;
			vertex.position.y = (float)currentNode->y;
			vertex.color = sf::Color::Magenta;
			pathLinesList.append(vertex);
		}

		if (displayDebug & SHOW_TEXT)
		{
			std::cout << currentNode->nodeID << ", ";
		}

		path.push_back(node);
		currentNode = currentNode->parent;
	}
	// Add last segment (starting point)
	if (doDrawing)
	{
		vertex.position.x = (float)currentNode->x;
		vertex.position.y = (float)currentNode->y;
		vertex.color = sf::Color::Magenta;
		pathLinesList.append(vertex);
	}

	if (displayDebug & SHOW_TEXT)
	{
		std::cout << _start->nodeID;
		std::cout << std::endl << "~Completed~" << std::endl << std::endl;
	}

	return path;
}

////////////////////////////////////////////////////////////////////////////////
void exportNodeGraph(std::vector<MainNode*> _list)
{
	unsigned int	cntAdj = 0;
	unsigned int	cntNode = 0;
	std::ofstream	outFile;
	sf::Time		saveTime;
	sf::Clock		timer;

	if (exportNodes)
	{
		timer.restart();
		outFile.open("main.nodegraph");

		outFile << "W" << std::endl;								//Save colour
		outFile << _list[_list.size() - 1]->nodeID << std::endl;	//Save highest node ID - use later to check validity of list
		outFile << _list.size() << std::endl;						//Save size of list

		//Save format: NodeID,x,y,SizeOfAdjancenciesList;AdjacentNodeID1,AdjacentNodeID2,AdjacentNodeID3,...AdjacentNodeIDn|		<- vertical bar character (NOT L OR 1) represents end of data for this node
		for (cntNode = 0; cntNode < nodesList.size(); cntNode++)
		{
			outFile << nodesList[cntNode]->nodeID << "," << std::fixed << std::setprecision(8) << nodesList[cntNode]->x << "," << nodesList[cntNode]->y << "," << nodesList[cntNode]->adjacentsList.size() << ";";
			for (cntAdj = 0; cntAdj < nodesList[cntNode]->adjacentsList.size(); cntAdj++)
			{
				if (cntAdj != 0)
				{
					outFile << "," << nodesList[cntNode]->adjacentsList[cntAdj]->nodeID;
				}
				else
				{
					outFile << nodesList[cntNode]->adjacentsList[cntAdj]->nodeID;
				}

			}
			outFile << "|";
		}
		outFile.close();

		// Get elapsed time
		saveTime = timer.getElapsedTime();
		if (displayDebug & SHOW_TEXT)
		{
			std::cout << "Finished exporting nodegraph in " << saveTime.asMilliseconds() << " milliseconds" << std::endl;
		}
	}

	return;
}

////////////////////////////////////////////////////////////////////////////////
void loadNodeGraph(std::string _nodeGraph)
{
	MainNode*			node = NULL;
	MainNode*			subNode = NULL;
	std::fstream		file;
	std::string			nodeGraphColour;
	unsigned int		maxNodeID = 0;
	unsigned int		numTotal = 0;
	unsigned int		cntNode = 0;
	unsigned int		cntRead = 0;
	unsigned int		cntAdj = 0;
	unsigned int		number = 0;
	size_t				pos = 0;
	std::string			str;
	std::string			value;
	std::string			content;
	sf::Clock			timer;

	// Open the list of files
	file.open(_nodeGraph);
	if (file.is_open())
	{
		file >> nodeGraphColour;
		file >> maxNodeID;
		file >> numTotal;

		// Read each line
		timer.restart();
		for (cntNode = 0; cntNode < 2; cntNode++)			//Go to 2, because the first loop will find the newline after numTotal
		{
			std::getline(file, str);
			std::stringstream	stream(str);

			while (std::getline(stream, value, '|'))			//bar character is end of node + adjancencies of node
			{
				if (value.length() > 0)
				{
					node = new MainNode;
					node->header = 0;
					for (cntRead = 0; cntRead < 4; cntRead++)	//4 readings for NodeID, x, y, SizeOfAdjanciesList
					{
						pos = value.find(',');
						content = value.substr(0, pos);
						if (cntRead == 0)
						{
							node->nodeID = atoi(content.c_str());
						}
						else if (cntRead == 1)
						{
							node->x = atof(content.c_str());
						}
						else if (cntRead == 2)
						{
							node->y = atof(content.c_str());
						}
						else if (cntRead == 3)						//Adjacents
						{
							number = atoi(content.c_str());			//Num of adjacents
							value.erase(0, value.find(';') + 1);
							for (cntAdj = 0; cntAdj < number; cntAdj++)
							{
								pos = value.find(',');
								content = value.substr(0, pos);
								subNode = new MainNode;
								subNode->nodeID = atoi(content.c_str());
								subNode->x = 0.0;
								subNode->y = 0.0;
								subNode->header = 0;
								node->adjacentsList.push_back(subNode);
								value.erase(0, value.find(',') + 1);
							}
						}
						value.erase(0, value.find(',') + 1);
					}
					nodesList.push_back(node);
				}
			}
		}

		// Validate nodeslist based on num of nodes + last node
		if (displayDebug & SHOW_TEXT)
		{
			std::cout << "Finished loading " << nodesList.size() << " nodes (out of " << numTotal << ") in " << timer.getElapsedTime().asMilliseconds() << " milliseconds" << std::endl;
		}

		// Always relink nodes after loading
		relinkNodes(nodesList);
	}

	return;
}

////////////////////////////////////////////////////////////////////////////////
void relinkNodes(std::vector<MainNode*> _nodesList)
{
	unsigned int	cntNode = 0;
	unsigned int	cntNext = 0;
	unsigned int	cntAdj = 0;
	unsigned int	cntSub = 0;
	sf::Clock		timer;
	timer.restart();

	if (displayDebug & SHOW_TEXT)
	{
		std::cout << "Relinking " << _nodesList.size() << " nodes...";
	}
	// For each node reconnect adjacenct nodes to main nodeslist
	for (cntNode = 0; cntNode < _nodesList.size(); cntNode++)
	{
		 // For each adjacent subnode
		for (cntAdj = 0; cntAdj < _nodesList[cntNode]->adjacentsList.size(); cntAdj++)
		{
			// Search for node with same Id in main nodesList
			for (cntNext = 0; cntNext < _nodesList.size(); cntNext++)
			{
				// If node was found, excepting itself
				if ((_nodesList[cntNext]->nodeID == _nodesList[cntNode]->adjacentsList[cntAdj]->nodeID) &&
					(cntNext != cntNode))
				{
					// Remove old adjacent subnode
					delete _nodesList[cntNode]->adjacentsList[cntAdj];
					// Add link to adjacenct node in main nodeslist
					_nodesList[cntNode]->adjacentsList[cntAdj] = _nodesList[cntNext];
					break;
				}
			}
		}
	}

	if (displayDebug & SHOW_TEXT)
	{
		std::cout << std::endl;
		std::cout << "Finished relinking " << _nodesList.size() << " nodes in " << timer.getElapsedTime().asMilliseconds() << " milliseconds" << std::endl;
	}

	return;
}

////////////////////////////////////////////////////////////////////////////////
