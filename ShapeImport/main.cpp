// Main.cpp : Defines the entry point for the console application.
//

#include "GlobalVariables.h"

// Forward declarations
ShapefileHeader				inputHeader(unsigned int _index);
int32_t						getInt(uint16_t _index, uint64_t _offset, bool _isBig);
long double					getDouble(uint16_t _index, uint64_t _offset);
std::vector<RecordHeader>	getRecords(unsigned int _index, ShapefileHeader _header);
bool						filterDouble(double val, double min, double max);
Point						readPoint(unsigned int _index, uint64_t _offset, bool _withShape);
PolyLine					readPolyLine(unsigned int _index, uint64_t _offset);
Polygon						readPolygon(unsigned int _index, uint64_t _offset);
void						handleWindow(void);
void						getDataList(void);
void						closeAllFiles(void);
bool						canCompress(double _initx, double _inity, double _compx, double _compy);
bool						notInList(unsigned int nodeID, std::vector<MainNode*> list);
void						generateNodes(unsigned int _index);
void						compressNodes(void);
void						saveNodesToFile(void);
//
sf::RenderWindow			mainWindow(sf::VideoMode(1280, 700), "Shapefile Displayer");
sf::View					mainView = mainWindow.getDefaultView();
//

std::vector<FILE*>				indexList;
std::vector<FILE*>				filesList;
std::vector<std::string>		fileNameList;
std::string						fileBase = "";
std::string						fileLocation = "../sh_files/";
char*							_fileName = "dataList.txt";
std::string						nodeGraph = "";

long							recValid = 0;
long							recDiscard = 0;

std::vector<Point>				pointList;
std::vector<PolyLine>			polyLineList;
std::vector<Polygon>			polyDataList;
std::vector<ShapefileHeader>	headerList;
std::vector<sf::Color>			colorList;
std::vector<sf::Texture>		textureList;
std::vector<MainNode*>			nodesList;
std::vector<sf::Sprite>			drawPointsList;
std::vector<LineSegment>		routesList;
sf::VertexArray					pathLinesList(sf::LinesStrip);

bool							exportNodes = false;
bool							doDrawing = false;
bool							doCompress = false;
bool							doSaveToFile = false;
bool							showNodes = false;

sf::Texture						pathCursorTexture;
sf::Sprite						pathStartCursor;
sf::Sprite						pathEndCursor;
sf::Clock						generalTimer;


////////////////////////////////////////////////////////////////////////////////
int TestSFML(void)
{
	sf::RenderWindow	window(sf::VideoMode(200, 200), "SFML works!");
	sf::CircleShape		shape(100.0);

	shape.setFillColor(sf::Color::Green);
	while (window.isOpen())
	{
		sf::Event	event;

		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		window.clear();
		window.draw(shape);
		window.display();
	}

	return 0;
}

////////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
	ShapefileHeader		header;
	int					cnt = 0;
	char				keyIn;

	std::cout << "Prepare export of nodes list to a file? [Y]/[N]";
	std::cin >> keyIn;
	if (keyIn == 'Y' || keyIn == 'y')
	{
		exportNodes = true;
	}
	else
	{
		exportNodes = false;
	}

	std::cout << "Compress overlapping nodes OR within a distance less than " << COMPRESS << " meter? [Y]/[N]";
	std::cin >> keyIn;
	if (keyIn == 'Y' || keyIn == 'y')
	{
		doCompress = true;
	}
	else
	{
		doCompress = false;
	}

	std::cout << "Save nodes list to a file? [Y]/[N]";
	std::cin >> keyIn;
	if (keyIn == 'Y' || keyIn == 'y')
	{
		doSaveToFile = true;
	}
	else
	{
		doSaveToFile = false;
	}

	std::cout << "Show nodes for pathfinding? [Y]/[N]";
	std::cin >> keyIn;
	if (keyIn == 'Y' || keyIn == 'y')
	{
		showNodes = true;
	}
	else
	{
		showNodes = false;
	}

	getDataList();

	for (cnt = 0; cnt < (int)filesList.size(); cnt++)
	{
		header = inputHeader(cnt);			//Create and define a header object
		headerList.push_back(header);

		if (displayDebug & SHOW_HEADER)
		{
			std::cout << "Loading shape file in list: '" << _fileName << "' ..." << std::endl << std::endl;
			std::cout << "Shape File: #" << cnt + 1 << "   File name: " << (fileLocation + fileNameList[cnt] + fileExtMain).c_str() << std::endl;
			std::cout << "***Header content***" << std::endl;
			std::cout << "\tFile Code: " << header.fileCode << std::endl;
			std::cout << "\tUnused: " << header.B04 << std::endl;
			std::cout << "\tUnused: " << header.B08 << std::endl;
			std::cout << "\tUnused: " << header.B12 << std::endl;
			std::cout << "\tUnused: " << header.B16 << std::endl;
			std::cout << "\tUnused: " << header.B20 << std::endl;
			std::cout << "\tFile Length: " << header.fileLength * 2 << " bytes" << std::endl;
			std::cout << "\tVersion: " << header.version << std::endl;
			std::cout << "\tShape Type: " << header.shapeType << std::endl;
			std::cout << "\tBounding: X min: " << std::to_string(header.minX) << std::endl;
			std::cout << "\tBounding: Y min: " << std::to_string(header.minY) << std::endl;
			std::cout << "\tBounding: X max: " << std::to_string(header.maxX) << std::endl;
			std::cout << "\tBounding: Y max: " << std::to_string(header.maxY) << std::endl;
			std::cout << "\tBounding: Z min: " << std::to_string(header.minZ) << std::endl;
			std::cout << "\tBounding: Z max: " << std::to_string(header.maxZ) << std::endl;
			std::cout << "\tBounding: M min: " << std::to_string(header.minM) << std::endl;
			std::cout << "\tBounding: M max: " << std::to_string(header.maxM) << std::endl;
			std::cout << "\tBounding: X dif: " << std::to_string(header.maxX - header.minX) << std::endl;
			std::cout << "\tBounding: Y dif: " << std::to_string(header.maxY - header.minY) << std::endl;
			std::cout << "Press Enter to continue..." << std::endl << std::endl;
			std::getchar();
		}

		getRecords(cnt, header);
		std::getchar();
	}

	//TestSFML();
	handleWindow();
	closeAllFiles();

	return 0;
}

////////////////////////////////////////////////////////////////////////////////
void handleWindow()
{
	sf::Vertex								vertex;
	LineSegment								segment;
	sf::Texture								markerTexture;
	sf::Texture								pointTexture;
	sf::Sprite								pointSprite;
	sf::Sprite								centerSprite;
	float			zoomScale = 1;
	unsigned int	cntHeader = 0;
	unsigned int	cntPoint = 0;
	unsigned int	cntLine = 0;
	unsigned int	cntPart = 0;
	unsigned int	cntNode = 0;
	unsigned int	cntRoute = 0;
	unsigned int	startIndex = 0;
	unsigned int	endIndex = 0;

	// Load marker
	if (!markerTexture.loadFromFile("marker.png"))
	{
		std::cout << "Error loading marker texture!" << std::endl;
		return;
	}
	centerSprite.setTexture(markerTexture);
	centerSprite.setPosition(mainView.getCenter());

	// Load square
	if (!pointTexture.loadFromFile("square8.png"))
	{
		std::cout << "Error loading point texture!" << std::endl;
		return;
	}
	pointSprite.setTexture(pointTexture);

	// Load path cursor
	if (!pathCursorTexture.loadFromFile("markerStar16.png"))
	{
		std::cout << "Error loading path cursor texture!" << std::endl;
		return;
	}
	pathStartCursor.setTexture(pathCursorTexture);
	pathStartCursor.setColor(sf::Color::Yellow);
	pathEndCursor.setTexture(pathCursorTexture);
	pathEndCursor.setColor(sf::Color::Green);

	// Loop for each file loaded
	for (cntHeader = 0; cntHeader < headerList.size(); cntHeader++)
	{
		// Initialize segment
		segment.header = cntHeader;
		segment.record = 0;
		segment.part = 0;
		segment.spriteList.clear();

		// Set color for vertex and sprite
		vertex.color = colorList[cntHeader];
		pointSprite.setColor(colorList[cntHeader]);

		if (headerList[cntHeader].shapeType == 1)
		{
			// Loop for each point loaded
			for (cntPoint = 0; cntPoint < pointList.size(); cntPoint++)
			{
				// Check if valid point belongs to this header
				if ((pointList[cntPoint].valid) &&
					(pointList[cntPoint].header == cntHeader))
				{
					// Note: vertical coordinate is from top to bottom, so we change its sign below
					pointSprite.setPosition((float)pointList[cntPoint].x, -(float)pointList[cntPoint].y);
					segment.spriteList.push_back(pointSprite);
				}
			}
			routesList.push_back(segment);
		}
		else if (headerList[cntHeader].shapeType == 3)
		{
			// Set array type to connected lines
			segment.vertexList.setPrimitiveType(sf::LinesStrip);

			// Loop for each polyline loaded
			for (cntLine = 0; cntLine < polyLineList.size(); cntLine++)
			{
				segment.record = cntLine;
				startIndex = 0;
				endIndex = polyLineList[cntLine].points.size();
				// Loop for each part loaded
				for (cntPart = 0; cntPart < polyLineList[cntLine].parts.size(); cntPart++)
				{
					segment.part = cntPart;
					// Get range for valid current part
					if (polyLineList[cntLine].parts[cntPart].valid)
					{
						startIndex = polyLineList[cntLine].parts[cntPart].index;
						if (cntPart + 1 < polyLineList[cntLine].parts.size())
						{
							if (polyLineList[cntLine].parts[cntPart + 1].valid)
							{
								endIndex = polyLineList[cntLine].parts[cntPart + 1].index;
							}
						}
					}
					// Initialize vertex list
					segment.vertexList.clear();
					// Loop for each point belonging to current part
					for (cntPoint = startIndex; cntPoint < endIndex; cntPoint++)
					{
						// Check if valid point belongs to this header
						if ((polyLineList[cntLine].points[cntPoint].valid) &&
							(polyLineList[cntLine].points[cntPoint].header == cntHeader))
						{
							// Note: vertical coordinate is from top to bottom, so we change its sign below
							vertex.position.x = (float)polyLineList[cntLine].points[cntPoint].x;
							vertex.position.y = -(float)polyLineList[cntLine].points[cntPoint].y;
							segment.vertexList.append(vertex);
						}
					}
					routesList.push_back(segment);
				}
			}
			// Generate the nodes list
			///Note: only for shape = 3 (streets & bus routes)
			generateNodes(cntHeader);
		}
		else if (headerList[cntHeader].shapeType == 5)
		{
			// Set array type to connected lines
			segment.vertexList.setPrimitiveType(sf::LinesStrip);

			// Loop for each polygon loaded
			for (cntLine = 0; cntLine < polyDataList.size(); cntLine++)
			{
				segment.record = cntLine;
				startIndex = 0;
				endIndex = polyDataList[cntLine].points.size();
				// Loop for each part loaded
				for (cntPart = 0; cntPart < polyDataList[cntLine].parts.size(); cntPart++)
				{
					segment.part = cntPart;
					// Get range for valid current part
					if (polyDataList[cntLine].parts[cntPart].valid)
					{
						startIndex = polyDataList[cntLine].parts[cntPart].index;
						if (cntPart + 1 < polyDataList[cntLine].parts.size())
						{
							if (polyDataList[cntLine].parts[cntPart + 1].valid)
							{
								endIndex = polyDataList[cntLine].parts[cntPart + 1].index;
							}
						}
					}
					// Initialize vertex list
					segment.vertexList.clear();
					// Loop for each point belonging to current part
					for (cntPoint = startIndex; cntPoint < endIndex; cntPoint++)
					{
						// Check if valid point belongs to this header
						if ((polyDataList[cntLine].points[cntPoint].valid) &&
							(polyDataList[cntLine].points[cntPoint].header == cntHeader))
						{
							// Note: vertical coordinate is from top to bottom, so we change its sign below
							vertex.position.x = (float)polyDataList[cntLine].points[cntPoint].x;
							vertex.position.y = -(float)polyDataList[cntLine].points[cntPoint].y;
							segment.vertexList.append(vertex);
						}
					}
					routesList.push_back(segment);
				}
			}
		}
	}
	// Load nodes from file
	if (nodeGraph != "")
	{
		loadNodeGraph(nodeGraph);
	}

	// Compress overlapping nodes
	compressNodes();
	// Save nodes list to file
	saveNodesToFile();
	// Export nodes to file
	exportNodeGraph(nodesList);
	// Show nodes for pathfinding
	if (showNodes)
	{
		pointSprite.setColor(sf::Color::White);
		for (cntNode = 0; cntNode < nodesList.size(); cntNode++)
		{
			pointSprite.setPosition((float)nodesList[cntNode]->x, (float)nodesList[cntNode]->y);
			drawPointsList.push_back(pointSprite);
		}
	}

	// Set initial centered position
	if (routesList.size() > 0)
	{
		if (routesList[0].vertexList.getVertexCount() > 0)
		{
			mainView.setCenter(routesList[0].vertexList[0].position);
		}
		else
		{
			mainView.setCenter(routesList[0].spriteList[0].getPosition());
		}
	}
	else if (drawPointsList.size() > 0)
	{
		mainView.setCenter(drawPointsList[0].getPosition());
	}
	else if (nodesList.size() > 0)
	{
		mainView.setCenter((float)nodesList[0]->x, (float)nodesList[0]->y);
	}
	mainWindow.setView(mainView);

	while (mainWindow.isOpen())
	{
		sf::Event	event;

		while (mainWindow.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				mainWindow.close();
			else if (event.type == sf::Event::Resized)
			{
				// update the view to the new size of the window
				sf::FloatRect visibleArea(0, 0, (float)event.size.width, (float)event.size.height);
				mainWindow.setView(sf::View(visibleArea));
			}
			else if (event.type == sf::Event::KeyPressed)
			{
				//Find points helper
				if (event.key.code == sf::Keyboard::C)
				{
					/*
					std::cout << "Demo: (" << centerSprite.getPosition().x << ", " << centerSprite.getPosition().y << ")" << std::endl;
					std::cout << "Points at:";

					for (cntPoint = 0; cntPoint < 3; cntPoint++)
					{
						std::cout << "(" << drawPointsList[cntPoint].getPosition().x << ", " << drawPointsList[cntPoint].getPosition().y << ")" << std::endl;
					}
					*/
				}
				if (event.key.code == sf::Keyboard::V)
				{
					if (routesList.size() > 0)
					{
						if (routesList[0].vertexList.getVertexCount() > 0)
						{
							mainView.setCenter(routesList[0].vertexList[0].position);
						}
						else
						{
							mainView.setCenter(routesList[0].spriteList[0].getPosition());
						}
					}
					else if (drawPointsList.size() > 0)
					{
						mainView.setCenter(drawPointsList[0].getPosition());
					}
					else if (nodesList.size() > 0)
					{
						mainView.setCenter((float)nodesList[0]->x, (float)nodesList[0]->y);
					}
				}
				// Show details for node under mouse
				else if (event.key.code == sf::Keyboard::D)
				{
					double			minDist = 0.0;
					double			calcDist = 0.0;
					unsigned int	nodeId = 0;
					sf::Vector2i	pixelPos = sf::Mouse::getPosition(mainWindow);
					sf::Vector2f	worldPos = mainWindow.mapPixelToCoords(pixelPos);

					// Search points to the index of the node in nodeslist closest to start position
					minDist = 10000000000;
					for (cntNode = 0; cntNode < nodesList.size(); cntNode++)
					{
						calcDist = getDistance(worldPos, nodesList[cntNode]);
						if (calcDist < minDist)
						{
							minDist = calcDist;
							nodeId = cntNode;
						}
					}

					if (displayDebug & SHOW_MOUSE)
					{
						std::cout << "NodeId: " << nodesList[nodeId]->nodeID;
						std::cout << "\tPixel: " << pixelPos.x << "," << pixelPos.y;
						std::cout << "\tCoord: " << nodesList[nodeId]->x << "," << nodesList[nodeId]->y;
						std::cout << "\tWorld: " << worldPos.x << "," << worldPos.y << std::endl;
					}
				}

				// Zooming the screen
				if (event.key.code == sf::Keyboard::Z)
				{
					zoomScale *= (float)0.4;
					mainView.zoom((float)0.4);
					mainWindow.setView(mainView);
				}
				else if (event.key.code == sf::Keyboard::X)
				{
					zoomScale *= 2.5;
					mainView.zoom(2.5);
					mainWindow.setView(mainView);
				}

				// Panning the screen
				else if (event.key.code == sf::Keyboard::Left)
				{
					mainView.move(-500 * zoomScale, 0);
					mainWindow.setView(mainView);
				}
				else if (event.key.code == sf::Keyboard::Right)
				{
					mainView.move(500 * zoomScale, 0);
					mainWindow.setView(mainView);
				}
				else if (event.key.code == sf::Keyboard::Up)
				{
					mainView.move(0, -500 * zoomScale);
					mainWindow.setView(mainView);
				}
				else if (event.key.code == sf::Keyboard::Down)
				{
					mainView.move(0, 500 * zoomScale);
					mainWindow.setView(mainView);
				}

				//Placing start and end path cursors
				else if (event.key.code == sf::Keyboard::Q)
				{
					sf::Vector2i pixelPos = sf::Mouse::getPosition(mainWindow);
					sf::Vector2f worldPos = mainWindow.mapPixelToCoords(pixelPos);
					pathStartCursor.setPosition(worldPos.x, worldPos.y);
					if (displayDebug & SHOW_CONTENT)
					{
						std::cout << "Pixel: " << pixelPos.x << ", " << pixelPos.y << std::endl;
						std::cout << "World: " << worldPos.x << ", " << worldPos.y << std::endl;
					}
				}
				else if (event.key.code == sf::Keyboard::W)
				{
					sf::Vector2i pixelPos = sf::Mouse::getPosition(mainWindow);
					sf::Vector2f worldPos = mainWindow.mapPixelToCoords(pixelPos);
					pathEndCursor.setPosition(worldPos.x, worldPos.y);
				}
				else if (event.key.code == sf::Keyboard::R)
				{
					doDrawing = !doDrawing;
				}
				///NOTE: pathStartCursor and pathEndCursor have a -y value instead of positive!
				else if (event.key.code == sf::Keyboard::E)
				{
					doDrawing = true;
					//Find shortest path between startpath and endpath

					//Start by finding the closest node in the nodes list for both start and end points
					generalTimer.restart();
					double			minDist = 0.0;
					double			calcDist = 0.0;
					unsigned int	startNodeId = 0;
					unsigned int	endNodeId = 0;

					// Search points to the index of the node in nodeslist closest to start position
					minDist = 10000000000;
					for (cntNode = 0; cntNode < nodesList.size(); cntNode++)
					{
						calcDist = getDistance(pathStartCursor, nodesList[cntNode]);
						if (calcDist < minDist)
						{
							minDist = calcDist;
							startNodeId = cntNode;
						}
					}
					// Search points to the index of the node in nodeslist closest to end position
					minDist = 10000000000;
					for (cntNode = 0; cntNode < nodesList.size(); cntNode++)
					{
						calcDist = getDistance(pathEndCursor, nodesList[cntNode]);
						if (calcDist < minDist)
						{
							minDist = calcDist;
							endNodeId = cntNode;
						}
					}
					// Now, startNodeId points to index of start MainNode 
					// and endNodeId points to index of end MainNode in nodeslist, so we pass these to aStar
					aStarPathFind(nodesList[startNodeId], nodesList[endNodeId]);
				}
			}
		}

		mainWindow.clear();

		// Draw helper
		centerSprite.setPosition(mainView.getCenter());
		mainWindow.draw(centerSprite);

		// Draw routes & sprites
		for (cntRoute = 0; cntRoute < routesList.size(); cntRoute++)
		{
			if (routesList[cntRoute].spriteList.size() > 0)
			{
				for (cntPoint = 0; cntPoint < routesList[cntRoute].spriteList.size(); cntPoint++)
				{
					mainWindow.draw(routesList[cntRoute].spriteList[cntPoint]);
				}
			}
			else if (routesList[cntRoute].vertexList.getVertexCount() > 0)
			{
				mainWindow.draw(routesList[cntRoute].vertexList);
			}
		}

		// Draw nodes
		for (cntPoint = 0; cntPoint < drawPointsList.size(); cntPoint++)
		{
			mainWindow.draw(drawPointsList[cntPoint]);
		}

		// Draw path
		mainWindow.draw(pathLinesList);

		mainWindow.draw(pathStartCursor);
		mainWindow.draw(pathEndCursor);

		mainWindow.display();
	}
}

////////////////////////////////////////////////////////////////////////////////
ShapefileHeader inputHeader(unsigned int _index)
{
	ShapefileHeader		_header;				//Local scope instance of a header object

	//Instantiate all fields (otherwise Visual Studio complains)
	memset (&_header, (int)0, sizeof (ShapefileHeader));

	_header.fileCode	= getInt(_index, 0, true);
	_header.B04			= getInt(_index, 4, true);
	_header.B08			= getInt(_index, 8, true);
	_header.B12			= getInt(_index, 12, true);
	_header.B16			= getInt(_index, 16, true);
	_header.B20			= getInt(_index, 20, true);
	_header.fileLength	= getInt(_index, 24, true);
	_header.version		= getInt(_index, 28, false);
	_header.shapeType	= getInt(_index, 32, false);
	_header.minX		= getDouble(_index, 36);
	_header.minY		= getDouble(_index, 44);
	_header.maxX		= getDouble(_index, 52);
	_header.maxY		= getDouble(_index, 60);
	_header.minZ		= getDouble(_index, 68);
	_header.maxZ		= getDouble(_index, 76);
	_header.minM		= getDouble(_index, 84);
	_header.maxM		= getDouble(_index, 92);

	// Check for invalid boundary
	if (_isnan(_header.minX))
		_header.minX = MINX;
	if (_isnan(_header.minY))
		_header.minY = MINY;
	if (_isnan(_header.maxX))
		_header.maxX = MAXX;
	if (_isnan(_header.maxY))
		_header.maxY = MAXY;

	return _header;
}

////////////////////////////////////////////////////////////////////////////////
// If using to calculate FileLength, remember to multiply the result by 2 to get the file size in bytes.
int32_t getInt(uint16_t _index, uint64_t _offset, bool _isBig)
{
	uint8_t	values[4] = {0, 0, 0, 0};
	int32_t	retval = 0;
	int		cnt = 0;

	_fseeki64(filesList[_index], _offset, SEEK_SET);
	for (cnt = 0; cnt < 4; cnt++)
	{
		values[cnt] = fgetc(filesList[_index]);
	}

	if (_isBig)
	{
		retval = values[0] * 256 * 256 * 256 + values[1] * 256 * 256 + values[2] * 256 + values[3];
	}
	else
	{
		retval = values[3] * 256 * 256 * 256 + values[2] * 256 * 256 + values[1] * 256 + values[0];
	}
	return retval;
}

////////////////////////////////////////////////////////////////////////////////
long double TestDouble(void)
{
	unsigned char	buffer[9];
	long double		retval = 0.0;

	// Credits: Reini Urban @perl.org
	// 3ff0 0000 0000 0000 = 1
	// 4000 0000 0000 0000 = 2
	// c000 0000 0000 0000 = -2
	// 8000 0000 0000 0000 = -0
	// 7ff0 0000 0000 0000 = Inf
	// fff0 0000 0000 0000 = -Inf
	// 3fd5 5555 5555 5555 = 1/3
	buffer[0] = 0x55;
	buffer[1] = 0x55;
	buffer[2] = 0x55;
	buffer[3] = 0x55;
	buffer[4] = 0x55;
	buffer[5] = 0x55;
	buffer[6] = 0xd5;
	buffer[7] = 0x3f;
	buffer[8] = NULL;
	memcpy(&retval, buffer, 8);

	return retval;
}

////////////////////////////////////////////////////////////////////////////////
long double getDouble(uint16_t _index, uint64_t _offset)				// All doubles are little endian
{
	unsigned char	buffer[9];		// 8 bytes + terminator
	long double		retval = 0.0;
	int				cnt = 0;

	_fseeki64(filesList[_index], _offset, SEEK_SET);
	memset(&buffer, NULL, sizeof(buffer));
	for (cnt = 0; cnt < 8; cnt++)			// direct reading
	//for (cnt = 7; cnt >= 0; cnt--)		// reverse reading
	{
		buffer[cnt] = fgetc(filesList[_index]);
	}
	//TestDouble();
	memcpy(&retval, buffer, sizeof(buffer) - 1);		// 8 bytes

	return retval;
}

////////////////////////////////////////////////////////////////////////////////
std::vector<RecordHeader> getRecords(unsigned int _index, ShapefileHeader _header)
{
	std::vector<RecordHeader>	_recHeadList;
	RecordHeader				_recordHeader;
	uint64_t					readBytePos = FILE_HEADER_LEN;
	Point						point;
	long						recCount = 0;
	long						sumContentLength = 0;
	double						fileSize = 0;

	// Reset the counters
	recValid = 0;
	recDiscard = 0;

	fileSize = (_header.fileLength * 2);
	while (readBytePos < fileSize)
	{
		recCount++;
		// Read record header
		_recordHeader.recordNumber = getInt(_index, readBytePos, true);
		_recordHeader.contentLength = getInt(_index, readBytePos + 4, true);
		_recHeadList.push_back(_recordHeader);
		sumContentLength += _recordHeader.contentLength;

		// Output Data
		if (displayDebug & SHOW_RECORD)
		{
			std::cout << "Record #" << _recordHeader.recordNumber << " Length: " << _recordHeader.contentLength;
			if (_recordHeader.recordNumber != recCount)
			{
				std::cout << "    Expected Record #" << recCount;
			}
			std::cout << std::endl;
		}

		// Set reading position for record data
		readBytePos += 8;

		if (_recordHeader.contentLength > 0)
		{
			// Handle different shape types here
			if (_header.shapeType == 1)
			{
				// Read point values at current position
				point = readPoint(_index, readBytePos, true);
				// Check if is a valid point
				if ((filterDouble(point.x, headerList[_index].minX, headerList[_index].maxX)) &&
					(filterDouble(point.y, headerList[_index].minY, headerList[_index].maxY)))
				{
					point.valid = true;
					recValid++;
				}
				else
				{
					point.valid = false;
					recDiscard++;
				}
				// Add point to array
				pointList.push_back(point);
			}
			else if (_header.shapeType == 3)
			{
				polyLineList.push_back(readPolyLine(_index, readBytePos));
			}
			else if (_header.shapeType == 5)
			{
				polyDataList.push_back(readPolygon(_index, readBytePos));
			}

			// Set reading position for next record
			readBytePos += _recordHeader.contentLength * 2;
		}
	}

	if (displayDebug & SHOW_TEXT)
	{
		std::cout << "Last reading position: " << readBytePos << std::endl;
		std::cout << "Total records read: " << _recHeadList.size() << std::endl;
		std::cout << "Valid points: " << recValid << "    Discarded points: " << recDiscard << std::endl;
		std::cout << "Total bytes read: " << FILE_HEADER_LEN << " + 8*" << _recHeadList.size() << " + 2*" << sumContentLength << " = " << FILE_HEADER_LEN + 8 * _recHeadList.size() + 2 * sumContentLength << std::endl;
	}

	return _recHeadList;
}

////////////////////////////////////////////////////////////////////////////////
bool filterDouble(double val, double min, double max)	// Returns true if value is inside interval
{
	// Check if is out-of-range or less than epsilon or is a NaN
	if ((val > max) ||
		(val < min) ||
		((val > -EPSILON) && (val < EPSILON)) ||
		(_isnan(val)))
	{
		return false;
	}
	else
	{
		return true;
	}
}

////////////////////////////////////////////////////////////////////////////////
Point readPoint(unsigned int _index, uint64_t _offset, bool _withShape)	//Shape=1
{
	Point	point;

	point.header = _index;
	if (_withShape)
	{
		point.shape = getInt(_index, _offset, false);
		point.x = getDouble(_index, _offset + 4);
		point.y = getDouble(_index, _offset + 12);
	}
	else
	{
		point.shape = 1;
		point.x = getDouble(_index, _offset);
		point.y = getDouble(_index, _offset + 8);
	}
	if (displayDebug & SHOW_POINTS)
	{
		std::cout << "\t\tRead point: " << point.x << ", " << point.y << " at byte " << _offset;
		// Check if is a valid point
		if ((filterDouble(point.x, headerList[_index].minX, headerList[_index].maxX)) &&
			(filterDouble(point.y, headerList[_index].minY, headerList[_index].maxY)))
		{
			std::cout << "\tValid" << std::endl;
		}
		else
		{
			std::cout << "\tDiscarded" << std::endl;
		}
	}

	return point;
}

////////////////////////////////////////////////////////////////////////////////
PolyLine readPolyLine(unsigned int _index, uint64_t _offset)	//Shape=3
{
	PolyLine	poly;
	Point		point;
	vpart		polyPart;
	int			cnt = 0;
	long		bytesFromPoints = 0;

	poly.shape = getInt(_index, _offset, false);
	poly.header = _index;
	for (cnt = 0; cnt < 4; cnt++)
	{
		// The Bounding Box for the PolyLine contains in order Xmin, Ymin, Xmax, Ymax
		poly.box[cnt] = getDouble(_index, _offset + 4 + 8 * cnt);
		if (displayDebug & SHOW_POINTS)
		{
			std::cout << "\tRead box[" << cnt << "] as " << poly.box[cnt] << std::endl;
		}
	}
	// Check for invalid boundary
	if (_isnan(poly.box[0]))
		poly.box[0] = headerList[_index].minX;
	if (_isnan(poly.box[1]))
		poly.box[1] = headerList[_index].minY;
	if (_isnan(poly.box[2]))
		poly.box[2] = headerList[_index].maxX;
	if (_isnan(poly.box[3]))
		poly.box[3] = headerList[_index].maxY;

	poly.numParts = getInt(_index, _offset + 36, false);
	poly.numPoints = getInt(_index, _offset + 40, false);

	if (displayDebug & SHOW_CONTENT)
	{
		std::cout << "\tNum Parts: " << poly.numParts << std::endl;
		std::cout << "\tNum Points: " << poly.numPoints << std::endl;
	}

	for (cnt = 0; cnt < poly.numParts; cnt++)
	{
		polyPart.index = getInt(_index, _offset + 44 + 4 * cnt, false);
		if ((poly.numPoints > 0) &&
			(polyPart.index >= 0) &&
			(polyPart.index < poly.numPoints))
		{
			polyPart.valid = true;
		}
		else
		{
			polyPart.valid = false;
		}
		poly.parts.push_back(polyPart);
		if (displayDebug & SHOW_POINTS)
		{
			std::cout << "\tRead part index " << cnt << std::endl;
		}
	}

	for (cnt = 0; cnt < poly.numPoints; cnt++)
	{
		// Read point values at current position
		point = readPoint(_index, _offset + 44 + 4 * poly.numParts + bytesFromPoints, false);
		point.shape = poly.shape;
		bytesFromPoints += 16;		// Point.struct = Double + Double = 8 + 8 = 16

		// Check if is a valid point inside poly.box
		if ((filterDouble(point.x, poly.box[0], poly.box[2])) &&
			(filterDouble(point.y, poly.box[1], poly.box[3])))
		{
			point.valid = true;
			recValid++;
		}
		else
		{
			point.valid = false;
			recDiscard++;
		}
		// Add point to array
		poly.points.push_back(point);

		if (displayDebug & SHOW_POINTS)
		{
			std::cout << "\t\tRead point index " << cnt << " with x: " << point.x << "   y: " << point.y << std::endl;
		}
	}

	return poly;
}

////////////////////////////////////////////////////////////////////////////////
Polygon readPolygon(unsigned int _index, uint64_t _offset)	//Shape=5
{
	Polygon		poly;
	Point		point;
	vpart		polyPart;
	int			cnt = 0;
	long		bytesFromPoints = 0;

	poly.shape = getInt(_index, _offset, false);
	poly.header = _index;
	for (cnt = 0; cnt < 4; cnt++)
	{
		// The Bounding Box for the Polygon contains in order Xmin, Ymin, Xmax, Ymax
		poly.box[cnt] = getDouble(_index, _offset + 4 + 8 * cnt);
		if (displayDebug & SHOW_POINTS)
		{
			std::cout << "\tRead box[" << cnt << "] as " << poly.box[cnt] << std::endl;
		}
	}
	// Check for invalid boundary
	if (_isnan(poly.box[0]))
		poly.box[0] = headerList[_index].minX;
	if (_isnan(poly.box[1]))
		poly.box[1] = headerList[_index].minY;
	if (_isnan(poly.box[2]))
		poly.box[2] = headerList[_index].maxX;
	if (_isnan(poly.box[3]))
		poly.box[3] = headerList[_index].maxY;

	poly.numParts = getInt(_index, _offset + 36, false);
	poly.numPoints = getInt(_index, _offset + 40, false);

	if (displayDebug & SHOW_CONTENT)
	{
		std::cout << "\tNum Parts: " << poly.numParts << std::endl;
		std::cout << "\tNum Points: " << poly.numPoints << std::endl;
	}

	for (cnt = 0; cnt < poly.numParts; cnt++)
	{
		polyPart.index = getInt(_index, _offset + 44 + 4 * cnt, false);
		if ((poly.numPoints > 0) &&
			(polyPart.index >= 0) &&
			(polyPart.index < poly.numPoints))
		{
			polyPart.valid = true;
		}
		else
		{
			polyPart.valid = false;
		}
		poly.parts.push_back(polyPart);
		if (displayDebug & SHOW_POINTS)
		{
			std::cout << "\tRead part index " << cnt << std::endl;
		}
	}

	for (cnt = 0; cnt < poly.numPoints; cnt++)
	{
		// Read point values at current position
		point = readPoint(_index, _offset + 44 + 4 * poly.numParts + bytesFromPoints, false);
		point.shape = poly.shape;
		bytesFromPoints += 16;		// Point.struct = Double + Double = 8 + 8 = 16

		// Check if is a valid point inside poly.box
		if ((filterDouble(point.x, poly.box[0], poly.box[2])) &&
			(filterDouble(point.y, poly.box[1], poly.box[3])))
		{
			point.valid = true;
			recValid++;
		}
		else
		{
			point.valid = false;
			recDiscard++;
		}
		// Add point to array
		poly.points.push_back(point);

		if (displayDebug & SHOW_POINTS)
		{
			std::cout << "\t\tRead point index " << cnt << " with x: " << point.x << "   y: " << point.y << std::endl;
		}
	}

	return poly;
}

////////////////////////////////////////////////////////////////////////////////
void getDataList()
{
	FILE*			pFile = NULL;
	std::string		lineContent;
	std::string		fileFull;
	std::ifstream	file;
	unsigned char	colorLetter = 'W';
	unsigned int	cnt = 0;

	// Open the list of files
	file.open(_fileName);
	if (file.is_open())
	{
		// Read each line
		while (std::getline(file, lineContent))
		{
			// Read up to first empty line
			if (lineContent.length() == 0)
				break;
			else
			{
				sf::Color	colorCode;

				// Check for the separator (an asterisk)
				if (strchr(lineContent.c_str(), (char)BUF_SEP) != NULL)
				{
					fileBase = lineContent.substr(0, lineContent.find((char)BUF_SEP));

					// Get color code
					colorLetter = lineContent[lineContent.find((char)BUF_SEP) + 1];
					switch (colorLetter)
					{
					case 'L':								// 'L' is for 'Load'
					case'l':								// NodeGraphs store their colour in the file
						nodeGraph = fileBase;
						colorCode = sf::Color::Magenta;
						break;
					case 'R':
					case 'r':
						colorCode = sf::Color::Red;
						break;
					case 'G':
					case 'g':
						colorCode = sf::Color::Green;
						break;
					case 'B':
					case 'b':
						colorCode = sf::Color::Blue;
						break;
					case 'M':
					case 'm':
						colorCode = sf::Color::Magenta;
						break;
					case 'C':
					case 'c':
						colorCode = sf::Color::Cyan;
						break;
					case 'Y':
					case 'y':
						colorCode = sf::Color::Yellow;
						break;
					case 'W':
					case 'w':
					default:
						//colorCode = sf::Color::White;
						colorCode.r = 127;
						colorCode.g = 127;
						colorCode.b = 127;
						break;
					}

				}
				else
				{
					fileBase = lineContent;
					colorLetter = 'W';
					colorCode = sf::Color::White;
				}

				// Skip files used for loading nodes
				if ((colorLetter != 'L') &&
					(colorLetter != 'l'))
				{
					fileNameList.push_back(fileBase);
					colorList.push_back(colorCode);
				}
			}
		}
		file.close();
	}
	else
	{
		std::cout << "Error reading data list file !\n";
		return;
	}

	for (cnt = 0; cnt < fileNameList.size(); cnt++)
	{
		// Open each shape file
		fileFull = fileLocation + fileNameList[cnt] + fileExtMain;
		pFile = fopen(fileFull.c_str(), "r");
		if (pFile == NULL)
		{
			perror("Error opening shape file!");
			return;
		}
		else
		{
			filesList.push_back(pFile);
		}
		// Now open the shape index
		fileFull = fileLocation + fileNameList[cnt] + fileExtIndex;
		pFile = fopen(fileFull.c_str(), "r");
		if (pFile == NULL)
		{
			perror("Error opening index file !");
			return;
		}
		else
		{
			indexList.push_back(pFile);
		}
	}

	return;
}

////////////////////////////////////////////////////////////////////////////////
void closeAllFiles(void)
{
	FILE*			pFile = NULL;
	unsigned int	cnt = 0;

	if (fileNameList.size() > 0)
	{
		for (cnt = 0; cnt < fileNameList.size(); cnt++)
		{
			if (filesList[cnt] != (FILE *)NULL)
				fclose(filesList[cnt]);
			if (indexList[cnt] != (FILE *)NULL)
				fclose(indexList[cnt]);
		}
	}

	return;
}

////////////////////////////////////////////////////////////////////////////////
